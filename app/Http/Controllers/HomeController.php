<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Task;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $get_task = Task::where('user_id',Auth::user()->id)->get();
        return view('home',compact('get_task'));
    }
    public function addevent(Request $request)
    {
        $this->validate($request,[
            'date'=>'required',
            'time'=>'required',
            'subject'=>'required',
        ]);
        $date = date("Y-m-d", strtotime($request->Input(['date'])));
        $time  = date("H:i", strtotime($request->Input(['time'])));
        $task = Task::where('date',$date)->Where('time',$time)->count();
        if($task > 0)
        {
        return back()->withError("This event time in ready is exist");
        }

        $new = new Task();
        $new->date = $date;
        $new->time = $time;
        $new->subject = $request->Input(['subject']);
        $new->user_id = Auth::user()->id;
        $new->save();
        return back()->withMessage("Successfully Create Event");
    }
    public function removeevent(Task $id)
    {
        $get = Task::find($id->id);
        $get->delete();
        return back()->withMessage("Successfully  Delete Event");

    }
}
