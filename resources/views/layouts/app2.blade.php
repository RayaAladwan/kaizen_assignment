<!DOCTYPE html>

<html lang="en">

<!-- begin::Head -->

<head>
    <meta charset="utf-8" />
    <title>Kizen | Dashboard</title>

    <!--begin::Web font -->
    <script src="{{ asset('admin/js/webfont.js') }}" type="text/javascript"></script>

    <script>
        WebFont.load({
            google: { "families": ["Poppins:300,400,500,600,700", "Roboto:300,400,500,600,700"] },
            active: function () {
                sessionStorage.fonts = true;
            }
        });
    </script>
    <style>
        label{
            word-wrap: break-word;

        }
        t1{
            font-weight: bold;
        }
        .addReadMore.showlesscontent .SecSec,
        .addReadMore.showlesscontent .readLess {
            display: none;
        }

        .addReadMore.showmorecontent .readMore {
            display: none;
        }

        .addReadMore .readMore,
        .addReadMore .readLess {
            margin-left: 2px;
            color: #34bfa3;
            cursor: pointer;
        }

        .addReadMoreWrapTxt.showmorecontent .SecSec,
        .addReadMoreWrapTxt.showmorecontent .readLess {
            display: block;
        }
        .add_remove{
            float: right;
            margin-top: -25px;
            margin-right: 5px;

        }

    </style>
    <!--end::Web font -->

    <!--begin::Global Theme Styles -->
    <link href="{{ asset('admin/assets/vendors/base/vendors.bundle.css') }}" rel="stylesheet" type="text/css" />

    <!--RTL version:<link href="assets/vendors/base/vendors.bundle.rtl.css" rel="stylesheet" type="text/css" />-->
    <link href="{{ asset('admin/assets/demo/demo6/base/style.bundle.css') }}" rel="stylesheet" type="text/css" />

    <!--RTL version:<link href="assets/demo/demo6/base/style.bundle.rtl.css" rel="stylesheet" type="text/css" />-->

    <!--end::Global Theme Styles -->

    <!--begin::Page Vendors Styles -->
    <link href="{{ asset('admin/assets/calendar/fullcalendar.css') }}" rel='stylesheet' />

    <!--RTL version:<link href="assets/vendors/custom/fullcalendar/fullcalendar.bundle.rtl.css" rel="stylesheet" type="text/css" />-->

    <!--end::Page Vendors Styles -->
    <!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>-->
    <script src="{{ asset('admin/js/jquery-3.3.1.min.js') }}" type="text/javascript"></script>
    <!--<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/jasny-bootstrap/3.1.3/css/jasny-bootstrap.min.css">-->
    <link rel="stylesheet" href="{{ asset('admin/css/jasny-bootstrap.min.css') }}">

    <link rel="stylesheet" href="{{ asset('admin/css/lightbox.min.css') }}">
    <link rel="stylesheet" href="{{ asset('admin/css/styleme.css') }}">
    <link href="{{ asset('icon/ionicons/css/ionicons.css') }}" rel="stylesheet">
    <link href="{{ asset('icon/elegant-icons/style.css') }}" rel="stylesheet">
    <link href="{{ asset('admin/vendors/select2/dist/css/select2.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('admin/assets/calendar/fullcalendar.css') }}" rel='stylesheet' />


    </head>

    <!-- end::Head -->

    <!-- begin::Body -->

    <body class="m-page--fluid m--skin- m-content--skin-light2 m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-light m-aside-left--fixed m-aside-left--offcanvas m-aside-left--minimize m-brand--minimize m-footer--push m-aside--offcanvas-default">

        <!-- begin:: Page -->
        <div class="m-grid m-grid--hor m-grid--root m-page">

            <!-- BEGIN: Header -->
            <header id="m_header" class="m-grid__item    m-header " m-minimize-offset="200" m-minimize-mobile-offset="200">
                <div class="m-container m-container--fluid m-container--full-height">
                    <div class="m-stack m-stack--ver m-stack--desktop">

                        <!-- BEGIN: Brand -->
                        <div class="m-stack__item m-brand  m-brand--skin-light " style="background-color: #e6eebc">
                            <div class="m-stack m-stack--ver m-stack--general">
                                <div class="m-stack__item m-stack__item--middle m-brand__logo">
                                    <a href="" class="m-brand__logo-wrapper">
                                    </a>

                                    <h3 class="m-header__title">Kizen</h3>
                                </div>
                                <div class="m-stack__item m-stack__item--middle m-brand__tools">

                                    <!-- BEGIN: Responsive Aside Left Menu Toggler -->
                                    <a href="javascript:;" id="m_aside_left_offcanvas_toggle" class="m-brand__icon m-brand__toggler m-brand__toggler--left m--visible-tablet-and-mobile-inline-block">
                                        <span></span>
                                    </a>

                                    <!-- END -->

                                    <!-- BEGIN: Responsive Header Menu Toggler -->
                                    <a id="m_aside_header_menu_mobile_toggle" href="javascript:;" class="m-brand__icon m-brand__toggler m--visible-tablet-and-mobile-inline-block">
                                        <span></span>
                                    </a>

                                    <!-- END -->

                                    <!-- BEGIN: Topbar Toggler -->
                                    <a id="m_aside_header_topbar_mobile_toggle" href="javascript:;" class="m-brand__icon m--visible-tablet-and-mobile-inline-block">
                                        <i class="flaticon-more"></i>
                                    </a>

                                    <!-- BEGIN: Topbar Toggler -->
                                </div>
                            </div>
                        </div>

                        <!-- END: Brand -->
                        <div class="m-stack__item m-stack__item--fluid m-header-head" id="m_header_nav">
                            <div class="m-header__title">
                                <h3 class="m-header__title-text" style="font-size: 25px;font-weight: bold;">Kizen</h3>
                            </div>
                            <!-- BEGIN: Topbar -->
                            <div id="m_header_topbar" class="m-topbar  m-stack m-stack--ver m-stack--general">

                                <div class="m-stack__item m-stack__item--middle m-dropdown m-dropdown--arrow m-dropdown--large m-dropdown--mobile-full-width m-dropdown--align-right m-dropdown--skin-light m-header-search m-header-search--expandable m-header-search--skin-light"
                                id="m_quicksearch" m-quicksearch-mode="default">

                                <!--BEGIN: Search Form -->
                                <form class="m-header-search__form" style="display:none;">
                                    <div class="m-header-search__wrapper">
                                        <span class="m-header-search__icon-search" id="m_quicksearch_search">
                                            <i class="flaticon-search"></i>
                                        </span>
                                        <span class="m-header-search__input-wrapper">
                                            <input autocomplete="off" type="text" name="q" class="m-header-search__input"
                                            value="" placeholder="Search..." id="m_quicksearch_input">
                                        </span>
                                        <span class="m-header-search__icon-close" id="m_quicksearch_close">
                                            <i class="la la-remove"></i>
                                        </span>
                                        <span class="m-header-search__icon-cancel" id="m_quicksearch_cancel">
                                            <i class="la la-remove"></i>
                                        </span>
                                    </div>
                                </form>

                                <!--END: Search Form -->

                                <!--BEGIN: Search Results -->
                                <div class="m-dropdown__wrapper">
                                    <div class="m-dropdown__arrow m-dropdown__arrow--center"></div>
                                    <div class="m-dropdown__inner">
                                        <div class="m-dropdown__body">
                                            <div class="m-dropdown__scrollable m-scrollable" data-scrollable="true"
                                            data-height="300" data-mobile-height="200">
                                            <div class="m-dropdown__content m-list-search m-list-search--skin-light">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <!--BEGIN: END Results -->
                        </div>
                        <div class="m-stack__item m-topbar__nav-wrapper">
                            <ul class="m-topbar__nav m-nav m-nav--inline">
                                <li class="m-nav__item m-topbar__user-profile  m-dropdown m-dropdown--medium m-dropdown--arrow  m-dropdown--align-right m-dropdown--mobile-full-width m-dropdown--skin-light"
                                m-dropdown-toggle="click">
                                <a href="#" class="m-nav__link m-dropdown__toggle">
                                    <span class="m-topbar__userpic m--hide">
                                    </span>
                                    <span class="m-nav__link-icon m-topbar__usericon">
                                        <span class="m-nav__link-icon-wrapper"><i class="flaticon-user-ok"></i></span>
                                    </span>
                                    <span class="m-topbar__username m--hide">{{ Auth::user()->name }}</span>
                                </a>
                                <div class="m-dropdown__wrapper">
                                    <span class="m-dropdown__arrow m-dropdown__arrow--right m-dropdown__arrow--adjust"></span>
                                    <div class="m-dropdown__inner">
                                        <div class="m-dropdown__header m--align-center">
                                            <div class="m-card-user m-card-user--skin-light">
                                                <div class="m-card-user__pic">
                                                </div>
                                                <div class="m-card-user__details">
                                                    <span class="m-card-user__name m--font-weight-500">
                                                        {{ Auth::user()->name }}
                                                    </span>
                                                    <a href="" class="m-card-user__email m--font-weight-300 m-link" style="word-break: break-word;">
                                                        {{ Auth::user()->email }}
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="m-dropdown__body">
                                            <div class="m-dropdown__content">
                                                <ul class="m-nav m-nav--skin-light">
                                                    <li class="m-nav__section m--hide">
                                                        <span class="m-nav__section-text">Section</span>
                                                    </li>
                                                    <li class="m-nav__separator m-nav__separator--fit">
                                                    </li>
                                                    <li class="m-nav__item">
                                                        <a href="{{ route('logout') }}" onclick="event.preventDefault();
                                                        document.getElementById('logout-form').submit();"
                                                        class="btn m-btn--pill    btn-secondary m-btn m-btn--custom m-btn--label-brand m-btn--bolder">
                                                        {{ __('Logout') }}
                                                    </a>
                                                    <form id="logout-form" action="{{ route('logout') }}"
                                                    method="POST" style="display: none;">
                                                    {{ csrf_field() }}
                                                </form>


                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
        </div>

        <!-- END: Topbar -->
    </div>
</div>
</div>
</header>

<!-- END: Header -->

<!-- begin::Body -->
<div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">

    <!-- BEGIN: Left Aside -->
    <button class="m-aside-left-close  m-aside-left-close--skin-light " id="m_aside_left_close_btn"><i class="la la-close"></i></button>
    <div id="m_aside_left" class="m-grid__item  m-aside-left  m-aside-left--skin-light ">

        <!-- BEGIN: Aside Menu -->
        <div id="m_ver_menu" class="m-aside-menu  m-aside-menu--skin-light m-aside-menu--submenu-skin-light " data-menu-vertical="true" m-menu-scrollable="1" m-menu-dropdown-timeout="500">
            <ul class="m-menu__nav  m-menu__nav--dropdown-submenu-arrow ">

            </ul>
        </div>

        <!-- END: Aside Menu -->
    </div>

    <!-- END: Left Aside -->
    <div class="m-grid__item m-grid__item--fluid m-wrapper">
        @yield('content')
    </div>
</div>

<!-- end:: Body -->

<!-- begin::Footer -->

<!-- end::Footer -->
</div>

<!-- end:: Page -->

<!-- begin::Quick Sidebar -->

<!-- end::Quick Sidebar -->

<!-- begin::Scroll Top -->
<div id="m_scroll_top" class="m-scroll-top">
    <i class="la la-arrow-up"></i>
</div>

<!-- end::Scroll Top -->

<!-- begin::Quick Nav -->

<!-- begin::Quick Nav -->


</body>
<script>
    $(function()
    {
        $('#theform').submit(function(){
            $("input[type='submit']", this)
            .val("Please Wait...")
            .attr('disabled', 'disabled');
            return true;
        });


    });

</script>
<!--begin::Global Theme Bundle -->
<script src="{{ asset('admin/assets/vendors/base/vendors.bundle.js') }}" type="text/javascript"></script>

<script src="{{ asset('admin/assets/demo/demo6/base/scripts.bundle.js') }}" type="text/javascript"></script>
<script src="{{ asset('admin/assets/calendar/fullcalendar.js') }}"></script>

<script src="{{ asset('admin/assets/app/js/dashboard.js') }}" type="text/javascript"></script>

<script src="{{ asset('admin/assets3/demo/default/custom/crud/forms/widgets/bootstrap-timepicker.js') }}" type="text/javascript"></script>
<script src="{{ asset('admin/assets3/demo/default/custom/crud/forms/widgets/input-mask.js') }}" type="text/javascript"></script>
<script src="{{ asset('admin/assets3/demo/default/custom/crud/forms/widgets/dropzone.js') }}" type="text/javascript"></script>
<script src="{{ asset('admin/assets3/demo/default/custom/crud/forms/widgets/bootstrap-timepicker.js') }}" type="text/javascript"></script>


<script src="{{ asset('admin/assets3/demo/default/custom/crud/forms/widgets/bootstrap-select.js') }}" type="text/javascript"></script>
<script src="{{ asset('admin/assets3/demo/default/custom/crud/forms/widgets/bootstrap-datepicker.js') }}" type="text/javascript"></script>
<script src="{{ asset('admin/assets3/demo/default/custom/crud/metronic-datatable/base/html-table.js') }}" type="text/javascript"></script>
<script src="{{ asset('admin/assets2/demo/custom/crud/forms/widgets/select2.js') }}" type="text/javascript"></script>

<!--end::Page Scripts -->
<noscript>Your browser does not support JavaScript!</noscript>
</html>