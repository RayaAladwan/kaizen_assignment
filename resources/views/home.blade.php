@extends('layouts.app2')
@section('content')


<script>
    var today = new Date();
    var dd = today.getDate();
var mm = today.getMonth()+1; //January is 0!
var yyyy = today.getFullYear();

if(dd<10) {
    dd = '0'+dd
} 

if(mm<10) {
    mm = '0'+mm
} 

today = mm + '/' + dd + '/' + yyyy;

document.addEventListener('DOMContentLoaded', function() {
    var calendarEl = document.getElementById('calendar');
    var calendar = new FullCalendar.Calendar(calendarEl, {
      header: {
        left: 'prev,next today',
        center: 'title',
        right: 'month,agendaWeek,agendaDay,listWeek'
    },
    buttonText: {
        listWeek: 'List',
    },
    defaultDate: today,
      navLinks: true, // can click day/week names to navigate views
      businessHours: true, // display business hours
      editable: false,
      eventLimit: true,
      allDay:false,
      eventClick: function(info) {
        var eventObj = info.event;
        if (eventObj.url) {
            alert(
              'Error'
              );
            window.open(eventObj.url);
        return true; // prevents browser from following link in current tab.
    } else {
        var result = confirm("Are you sure Delete event ?");
        if (result) {
            window.location.replace('removeevent/'+eventObj.id);
        }
    }
},

    events: [
      <?php
      $color = ['#257e4a','#ff9f89','#EC4E3C'];
      foreach ($get_task as $key) {
        echo "{
          title: '".$key->subject."',
          id: '".$key->id."',
          start: '".$key->date."T".$key->time."',
          constraint: 'businessHours',
          description: '".$key->id."',
          color:'".$color[array_rand($color)]."'
      },";
      }
      ?>
      ]


    });
    calendar.render();
});

</script>
<script type="text/javascript">
    $(function() {
        $( "#datepicker" ).datepicker({
            autoclose: true,
            format: 'yyyy-mm-dd',
            startDate:'-0d',
        });
    });

    

</script>


<div class="m-subheader">
    <div class="d-flex align-items-center">
        <div class="mr-auto">
            <h3 class="m-subheader__title m-subheader__title--separator">Calendar</h3>
            <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                <li class="m-nav__item m-nav__item--home">
                    <a href="{{action('HomeController@index')}}" class="m-nav__link m-nav__link--icon">
                        <i class="m-nav__link-icon la la-home"></i>
                    </a>
                </li>
                <li class="m-nav__separator">-</li>
                <li class="m-nav__item">
                    <a href="" class="m-nav__link">
                        <span class="m-nav__link-text">Calendar</span>
                    </a>
                </li>

            </ul>
        </div>
    </div>
</div>

<div class="m-content">
    @if(Session::has('message'))
    <div class="alert alert-success">
      {{ Session::get('message') }}
  </div>
  @endif
  @if(count($errors))
  @foreach($errors->all() as $error)
  <div class="alert alert-danger ">
    {{ $error }}
</div>
@endforeach
@endif


<div class="m-portlet " id="m_portlet">

    <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
                <span class="m-portlet__head-icon">
                    <i class="flaticon-calendar"></i>
                </span>
                <h3 class="m-portlet__head-text">
                    Calendar
                </h3>
            </div>
        </div>
        <div class="m-portlet__head-tools">
          <ul class="m-portlet__nav">
            <li class="m-portlet__nav-item">
                <button type="button" class="btn btn-accent m-btn m-btn--custom m-btn--icon m-btn--pill m-btn--air" data-toggle="modal" data-target="#m_modal_1">
                  <i class="la la-plus"></i>
                  Add Event
              </button>
          </li>

      </ul>
  </div>
</div>
<div class="m-portlet__body">
    <div id="calendar"></div>
</div>
</div>

<!--end::Portlet-->
</div>



<div class="modal fade" id="m_modal_1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Add Event</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true" class="la la-remove"></span>
      </button>
  </div>
  <form class="m-form m-form--fit m-form--label-align-right "method="POST" action="{{action('HomeController@addevent')}}"
  role="form"  id='theform'>
  {{ csrf_field() }}
  <div class="modal-body">
      <div class="form-group m-form__group row m--margin-top-20">
        <label class="col-form-label col-lg-3 col-sm-12">Subject</label>
        <div class="col-lg-9 col-md-9 col-sm-12">
            <textarea class="form-control m-input m-input--air m-input--pill" id="message-text" maxlength="255" name="subject" required></textarea>
            {!! $errors->first('subject', '<p class="help-block"  style="color: red">:message</p>') !!}
        </div>
    </div>
    <div class="form-group m-form__group row">
        <label class="col-form-label col-lg-3 col-sm-12">Date</label>
        <div class="col-lg-9 col-md-9 col-sm-12">
          <div class="input-group date">
            <input type="text" class="form-control m-input m-input--air m-input--pill" name="date" readonly placeholder="Select date" id="datepicker"  required  />
            <div class="input-group-append">
              <span class="input-group-text">
                <i class="la la-calendar-check-o"></i>
            </span>
        </div>
    </div>
    {!! $errors->first('date', '<p class="help-block"  style="color: red">:message</p>') !!}
</div>
</div>
<div class="form-group m-form__group row m--margin-bottom-20">
    <label class="col-form-label col-lg-3 col-sm-12">Time</label>
    <div class="col-lg-9 col-md-9 col-sm-12">

      <div class="input-group timepicker">
          <input class="form-control m-input m-input--air m-input--pill" id="m_timepicker_1" name="time" readonly placeholder="Select time" type="text" required/>
          <div class="input-group-append">
              <span class="input-group-text">
                <i class="la la-clock-o"></i>
            </span>
        </div>
    </div>
    {!! $errors->first('time', '<p class="help-block"  style="color: red">:message</p>') !!}
</div>
</div>
</div>
<div class="modal-footer">
    <input class='btn btn-accent' type="submit" value="Submit" id='btnsubmit' />   
    <button type="reset" class="btn btn-default">Reset</button>
</div>
</form>
</div>
</div>
</div>

@endsection